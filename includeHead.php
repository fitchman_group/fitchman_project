<?php include('includeFunctions.php'); ?>
<?php
  
  //** Global Page Variables */
  $phoneNumber = '800-555-1234';
  $phoneLinkNumber = str_replace('-','',$phoneNumber);
    
  $ctaButtonLabel = 'Get Started Now';
  $slogan = '';
  $callLine = 'Call to Schedule Your Free Consultation:';


  $baseUrl = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include('includeGtm.php'); ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="<?=isset($description) ? $description : ''?>">
    <meta name="keywords" content="<?=isset($keywords) ? $keywords : ''?>">
    <meta name="author" content="">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?=$baseUrl?>">
    <meta property="og:title" content="<?=isset($title) ? $title : ''?>">
    <meta property="og:description" content="<?=isset($description) ? $description : ''?>">
    <meta property="og:image" content="<?=$baseUrl?>images/hero-background.jpg">

    <title><?=isset($title) ? $title : ''?></title>
    <script src="https://kit.fontawesome.com/8e05e8780b.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  
    <!-- Bootstrap core CSS -->
    <link href="<?=fileWithTimeStamp('/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=fileWithTimeStamp('/css/styles.css')?>" rel="stylesheet">

    <?php include('includeFavicon.php'); ?>
  </head>
