<?php

  $description = 'Selling Your Home? Call for a free consultation. We are available days, nights and weekends.';
  $title = 'Top Rated Agent in Las Vegas | Earl White, LLC';
  $keywords = '';

  include('includeHead.php');
  
?>
  <body id="terms">
  <?php include('includeHeader.php'); ?>
  <main role="main">
    <!-- Hero Row -->
    <header class="hero-row" id="get-started">
      <div class="container">
          <div class="row d-flex align-items-center my-5">
              <div class="col-12 text-center">
                  <h1 class="font-weight-bold text-white text-center">Privacy Policy</h1>
              </div>
          </div>
      </div>
    </header>


    <div class="container">

      <div class="row">
        <div class="col-12">               
<p>Protecting your private information is our priority. This Statement of Privacy applies to earlwrealtor.com and Earl White LLC and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Earl White LLC include earlwrealtor.com and Earl White LLC. The Earl White LLC website is a Real Estate Web site site. By using the Earl White LLC website, you consent to the data practices described in this statement.</p>
<h2>Collection of your Personal Information</h2>
<p>We do not collect any personal information about you unless you voluntarily provide it to us. However, you may be required to provide certain personal information to us when you elect to use certain products or services available on the Site. These may include: (a) registering for an account on our Site; (b) entering a sweepstakes or contest sponsored by us or one of our partners; (c) signing up for special offers from selected third parties; (d) sending us an email message; (e) submitting your credit card or other payment information when ordering and purchasing products and services on our Site. To wit, we will use your information for, but not limited to, communicating with you in relation to services and/or products you have requested from us. We also may gather additional personal or non-personal information in the future.</p>
<h2>Sharing Information with Third Parties</h2>
<p>Earl White LLC does not sell, rent or lease its customer lists to third parties.</p>
<p>Earl White LLC may share data with trusted partners to help perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to Earl White LLC, and they are required to maintain the confidentiality of your information.</p>
<p>Earl White LLC may disclose your personal information, without notice, if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Earl White LLC or the site; (b) protect and defend the rights or property of Earl White LLC; and/or (c) act under exigent circumstances to protect the personal safety of users of Earl White LLC, or the public.</p>
<h2>Automatically Collected Information</h2>
<p>Information about your computer hardware and software may be automatically collected by Earl White LLC. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, tomaintain quality of the service, and to provide general statistics regarding use of the Earl White LLC website.</p>
<h2>Use of Cookies</h2>
<p>The Earl White LLC website may use "cookies" to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.</p>
<p>One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize Earl White LLC pages, or register with Earl White LLC site or services, a cookie helps Earl White LLC to recall your specific information on subsequent visits. This simplifies the process of recording your personal information, such as billing addresses, shipping addresses, and so on. When you return to the same Earl White LLC website, the information you previously provided can be retrieved, so you can easily use the Earl White LLC features that you customized.</p>
<p>You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the Earl White LLC services or websites you visit.</p>
<h2>Security of your Personal Information</h2>
<p>Earl White LLC secures your personal information from unauthorized access, use, or disclosure. Earl White LLC uses the following methods for this purpose:</p>
<p>-- SSL Protocol</p>
<p>When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol.</p>
<p>We strive to take appropriate security measures to protect against unauthorized access to or alteration of your personal information. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, you acknowledge that: (a) there are security and privacy limitations inherent to the Internet which are beyond our control; and (b) security, integrity, and privacy of any and all information and data exchanged between you and us through this Site cannot be guaranteed.</p>
<h2>Right to Deletion</h2>
<p>Subject to certain exceptions set out below, on receipt of a verifiable request from you, we will:</p>
<ul>
  <li>Delete your personal information from our records; and</li>
  <li>Direct any service providers to delete your personal information from their records.</li>
</ul>
<p>Please note that we may not be able to comply with requests to delete your personal information if it is necessary to:</p>
<ul>
  <li>Complete the transaction for which the personal information was collected, fulfill the terms of a written warranty or product recall conducted in accordance with federal law, provide a good or service requested by you, or reasonably anticipated within the context of our ongoing business relationship with you, or otherwise perform a contract between you and us;</li>
  <li>Detect security incidents, protect against malicious, deceptive, fraudulent, or illegal activity; or prosecute those responsible for that activity;</li>
  <li>Debug to identify and repair errors that impair existing intended functionality;</li>
  <li>Exercise free speech, ensure the right of another consumer to exercise his or her right of free speech, or exercise another right provided for by law;</li>
  <li>Comply with the California Electronic Communications Privacy Act;</li>
  <li>Engage in public or peer-reviewed scientific, historical, or statistical research in the public interest that adheres to all other applicable ethics and privacy laws, when our deletion of the information is likely to render impossible or seriously impair the achievement of such research, provided we have obtained your informed consent;</li>
  <li>Enable solely internal uses that are reasonably aligned with your expectations based on your relationship with us;</li>
  <li>Comply with an existing legal obligation; or</li>
  <li>Otherwise use your personal information, internally, in a lawful manner that is compatible with the context in which you provided the information.</li>
</ul>
<h2>Children Under Thirteen</h2>
<p>Earl White LLC does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this website.</p>
<h2>E-mail Communications</h2>
<p>From time to time, Earl White LLC may contact you via email for the purpose of providing announcements, promotional offers, alerts, confirmations, surveys, and/or other general communication. In order to improve our Services, we may receive a notification when you open an email from Earl White LLC or click on a link therein.
<p>If you would like to stop receiving marketing or promotional communications via email from Earl White LLC, you may opt out of such communications by clicking on the UNSUBSCRIBE button.</p>

<h2>Changes to this Statement</h2>
<p>Earl White LLC reserves the right to change this Privacy Policy from time to time. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your account, by placing a prominent notice on our site, and/or by updating any privacy information on this page. Your continued use of the Site and/or Services available through this Site after such modifications will constitute your: (a) acknowledgment of the modified Privacy Policy; and (b) agreement to abide and be bound by that Policy.</p>

<h2>Contact Information</h2>
<p>Earl White LLC welcomes your questions or comments regarding this Statement of Privacy. If you believe that Earl White LLC has not adhered to this Statement, please contact Earl White LLC at:</p>

<p>Earl White LLC<br />
3185 St. Rose Parkway #100<br />
Henderson, Nevada 89052</p>

<p>Email Address:<br />
earlwrealtor@gmail.com</p>

<p>Telephone number:<br />
7024658869</p>

<p>Effective as of June 22, 2020</p>

        </div>
      </div>

    </div>
    <!-- /.container -->
  </main>

    <?php include('includeFooter.php'); ?>

  </body>

</html>
