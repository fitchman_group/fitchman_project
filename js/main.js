$(document).ready(function() {

    $('a[href^="tel:"]').on('click', function(e) {
        if(window.matchMedia('(min-width: 768px)').matches) {
            e.preventDefault();
            return false;
        }
    });

    var toggleAffix = function(affixElement, scrollElement, wrapper) {

        //var height = affixElement.outerHeight(),
        var height = 0,
            top = wrapper.offset().top;

        if (scrollElement.scrollTop() >= top){
            wrapper.height(height);
            affixElement.addClass("affix");
        }
        else {
            affixElement.removeClass("affix");
            wrapper.height('auto');
        }

    };


    $('[data-toggle="affix"]').each(function() {
        var ele = $(this),
            wrapper = $('<div></div>');

        ele.before(wrapper);
        $(window).on('scroll resize', function() {
            toggleAffix(ele, $(this), wrapper);
        });

        // init
        toggleAffix(ele, $(window), wrapper);
    });

    $('.nav-link, .sub-nav-link').on('click', function(e) {
        if(window.matchMedia('(max-width: 992px)').matches) {
            if($(this).next('.sub-nav').length || $(this).next('.sub-sub-nav').length) {
                e.preventDefault();
                if($(this).next('.sub-nav').length) {
                    if($(this).hasClass('is-expanded')) {
                        $(this).next('.sub-nav').slideUp('fast');
                        $(this).removeClass('is-expanded');
                    } else {
                        $('.navbar-nav .is-expanded').removeClass('is-expanded');
                        $(this).addClass('is-expanded');
                        $('.sub-nav, .sub-sub-nav').slideUp('fast');
                        $(this).next('.sub-nav').slideDown('fast');
                    }
                } else {
                    if($(this).hasClass('is-expanded')) {
                        $(this).next('.sub-sub-nav').slideUp('fast');
                        $(this).removeClass('is-expanded');
                    } else {
                        $('.navbar-nav .sub-nav-link.is-expanded').removeClass('is-expanded');
                        $(this).addClass('is-expanded');
                        $('.sub-sub-nav').slideUp('fast');
                        $(this).next('.sub-sub-nav').slideDown('fast');
                    }
                }
            } else {
                $('.navbar-nav .is-expanded').removeClass('is-expanded');
                $('.sub-nav, .sub-sub-nav').hide();
                $('.navbar-toggler').click();
            }
        }
    });

    $(window).on('resize', function() {
        if(window.matchMedia('(min-width: 992px)').matches) {
            $('.sub-nav').hide();
        }
    });

    fixedHeightCarousel();

    $(window).on('resize', function() {
        fixedHeightCarousel();
    });

});

function setNav(){

    if($(window).scrollTop()>0 && !isScrolled){
        isScrolled=true;
        $( "nav" ).stop().animate({top: 0}, 200,function(){
            $( "nav" ).css('top','0');
        });
        $( ".header" ).stop().animate({top: -37}, 200,function(){
            $( ".header" ).css('top','-37px');
        });

    }else if($(window).scrollTop()==0 && isScrolled){
        isScrolled=false;
        $('.header').stop().css('top','-37px').animate({top: 0}, 200,function(){
            $('.header-').css('top','0');
        });
        $('nav').stop().css('top','0').animate({top: 37}, 200,function(){
            $('nav').css('top','37px');
        });



    }
}

function fixedHeightCarousel() {
    var car = $('#carouselContent');
    var h = 0;
    $('.carousel-item',car).each(function() {
        if($(this).outerHeight()>h) {
            h = $(this).outerHeight();
        }
    });

    car.css('min-height',h+'px');
}


function iOS() {
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform);
}
