if (location.hash) {
    setTimeout(function() {
        window.scrollTo(0, 0);
    }, 1);
}

$(function() {

// Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#carouselContent"]')
        .not('[href="#carouselExampleIndicators"]')
        .not('[href="#0"]')
        .not('.no-scroll')
        .click(function(event) {
          
            var target = $(this.hash);
            var targetOffset;
            var adjust = 0;
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

            var hasHeroForm = ((target.parents('.hero-row').length>0 || target.hasClass('hero-row')) && $('form',target).length > 0);

            if($(this).parents('.nav-item').length && window.matchMedia('(max-width: 1200px)')) {
                adjust = $('#navbarSupportedContent').outerHeight();
                $('.navbar-toggler').click();
            }
            
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                var navBarHeight = $('.navbar').height();

                //for mobile
                if(window.matchMedia('(max-width: 1200px)').matches) {
                    targetOffset = target.offset().top - adjust;
                //for desktop
                } else {
                    if(hasHeroForm) {
                        targetOffset = 0;
                    } else {
                        targetOffset = target.offset().top - navBarHeight;
                    }
                }

                $([document.documentElement, document.body]).animate({
                    scrollTop: targetOffset
                }, 1000, function() {

                    if(window.matchMedia('(min-width: 992px)').matches && hasHeroForm) {
                        $('form',target).first()[0].reportValidity();
                    }

                });
            }
        });


    // Anchor link from separate page
    if(window.location.hash) {

        var hash = location.hash;
        if (hash && $(hash).length) {

            var target = $(hash).offset().top;

            if($(window).width() > 1199) {
                var targetOffset = target - $('.bg-light').height();
            } else {
                var targetOffset = target /* - ( ('.bg-light').height() 120 + $('#top').height() )*/;
            }

            // smooth scroll to the anchor id
            setTimeout(function() {
                $('html, body').animate({
                    scrollTop: targetOffset
                }, 1000, 'swing');
            }, 500);
        }
    }

});