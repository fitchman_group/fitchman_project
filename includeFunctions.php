<?php
  
  /**
   * Creates a query string with a timestamp to bust cache
   * when CSS or JS files have been changed
   */
  function fileWithTimeStamp ($filename) {
      return $filename . '?' . filemtime($_SERVER["DOCUMENT_ROOT"] . '/'.$filename);
  }

  function baseUrl(){
    return sprintf(
      "%s://%s",
      isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
      $_SERVER['SERVER_NAME']
    );
  }