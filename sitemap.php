<?php


  $description = 'Selling Your Home? Call for a free consultation. We are available days, nights and weekends.';
  $title = 'Top Rated Agent in Las Vegas | Earl White, LLC';
  $keywords = '';

  include('includeHead.php');
  
?>
  <body id="sitemap">
  <?php include('includeHeader.php'); ?>
  <main role="main">
    <!-- Hero Row -->
    <header class="hero-row" id="get-started">
      <div class="container">
        <div class="row d-flex align-items-center my-5">
          <div class="col-12">
            <h1 class="font-weight-bold text-white text-center">Sitemap</h1>
          </div>
        </div>
      </div>
    </header>

    <!-- Choose Your Plan Row -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-sm-12">
        <br />
          <ul>
            <li><p><a href="/">Home</a></p></li>
            <li><p><p><a href="/sitemap.php">Sitemap</a></p></li>
          </ul>
          </div>
      </div>

    </div>
    <!-- /.container -->
  </main>

    <?php include('includeFooter.php'); ?>

  </body>

</html>
